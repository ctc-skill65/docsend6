<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$search = get('search'); 

if (!empty($search)) {
    $items = DB::result("SELECT 
    `docs`.* ,
    `doc_types`.`doc_type_name`,
    `users`.`firstname`,
    `users`.`lastname`,
    `users`.`email`,
    to_user.firstname AS to_firstname,
    to_user.lastname AS to_lastname,
    to_user.email AS to_email,
    to_dept.dept_name AS to_dept_name
    FROM `docs` 
    LEFT JOIN `doc_types` ON `doc_types`.`doc_type_id`=`docs`.`doc_type_id`
    LEFT JOIN `users` ON `users`.`user_id`=`docs`.`user_id`
    LEFT JOIN `users` AS to_user ON to_user.user_id=`docs`.`to_user_id`
    LEFT JOIN `depts` AS to_dept ON to_dept.dept_id=`docs`.`to_dept_id`
    WHERE CONCAT(
        `docs`.`doc_name`, ' ',
        `users`.`firstname`, ' ', 
        `users`.`lastname`, ' ',
        `docs`.`send_time`
    ) LIKE '%{$search}%'");
} else {
    $items = DB::result("SELECT 
    `docs`.* ,
    `doc_types`.`doc_type_name`,
    `users`.`firstname`,
    `users`.`lastname`,
    `users`.`email`,
    to_user.firstname AS to_firstname,
    to_user.lastname AS to_lastname,
    to_user.email AS to_email,
    to_dept.dept_name AS to_dept_name
    FROM `docs` 
    LEFT JOIN `doc_types` ON `doc_types`.`doc_type_id`=`docs`.`doc_type_id`
    LEFT JOIN `users` ON `users`.`user_id`=`docs`.`user_id`
    LEFT JOIN `users` AS to_user ON to_user.user_id=`docs`.`to_user_id`
    LEFT JOIN `depts` AS to_dept ON to_dept.dept_id=`docs`.`to_dept_id`
    ORDER BY `docs`.`doc_id` ASC");
}
ob_start();
?>
<?= showAlert() ?>
<form method="get">
    <input type="search" name="search" id="search">
    <button type="submit">ค้นหา</button>
</form>
<table>
    <thead>
        <tr> 
            <th>รหัส</th>
            <th>ชื่อเรื่อง</th>
            <th>ประเภทเอกสาร</th>
            <th>ผู้ส่ง</th>
            <th>ประเภทการส่ง</th>
            <th>ส่งถึง</th>
            <th>วันเวลาส่งเอกสาร</th>
            <th>สถานะการอ่าน</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['doc_id'] ?></td>
                <td><?= $item['doc_name'] ?></td>
                <td><?= $item['doc_type_name'] ?></td>
                <td><?= $item['firstname'] . ' ' . $item['lastname'] ?> (<?= $item['email'] ?>)</td>
                <td>
                <?php
                switch ($item['send_type']) {
                    case 'user':
                        echo 'ส่งให้ผู้ใช้งาน';
                        break;

                    case 'dept':
                        echo 'ส่งให้หน่วยงาน';
                        break;
                }
                ?>
                </td>
                <td>
                <?php
                switch ($item['send_type']) {
                    case 'user':
                        echo "{$item['to_firstname']} {$item['to_lastname']} ({$item['to_email']})";
                        break;

                    case 'dept':
                        echo "หน่วยงาน: " . $item['to_dept_name'];
                        break;
                }
                ?>
                </td>
                <td><?= $item['send_time'] ?></td>
                <td>
                <?php
                switch ($item['read_status']) {
                    case '1':
                        echo 'อ่านแล้ว';
                        break;

                    case '0':
                        echo 'ยังไม่อ่าน';
                        break;
                }
                ?>
                </td>
                <td>
                    <a href="<?= url("/guest/download.php?id={$item['doc_id']}") ?>" target="_blank" rel="noopener noreferrer">
                        ดาวน์โหลด
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'ข้อมูลการส่งเอกสาร';
require ROOT . '/admin/layout.php';
