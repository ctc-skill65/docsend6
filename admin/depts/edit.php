<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$dept_id = get('id');
$page_path = "/admin/depts/edit.php?id={$dept_id}";

if ($_POST) {
    $result = DB::update('depts', [
        'dept_name' => post('dept_name')
    ], "`dept_id`='{$dept_id}'");

    if ($result) {
        setAlert('success', "แก้ไขหน่วยงานต่างๆสำเร็จเรียบร้อย");
        redirect('/admin/depts/list.php');
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขหน่วยงานต่างๆได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `depts` WHERE `dept_id`='{$dept_id}'");
ob_start();
?>
<?= showAlert() ?>

<form method="post">
    <label for="dept_name">ชื่อหน่วยงานต่างๆ</label>
    <input type="text" name="dept_name" id="dept_name" value="<?= $data['dept_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขหน่วยงานต่างๆ';
require ROOT . '/admin/layout.php';
