<?php

return [
    'app_name' => 'ระบบรับส่งเอกสารออนไลน์',
    'site_url' => 'http://skill65.local/docSend6',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_docSend6',
    'db_charset' => 'utf8mb4'
];
