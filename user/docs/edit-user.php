<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$doc_id = get('doc');
$page_path = "/user/docs/edit-user.php?doc={$doc_id}";

if ($_POST) {
    if (!empty($_FILES['file']['name'])) {
        $file = upload('file');
        if (!$file) {
            setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์ได้");
            redirect($page_path);
        }

        DB::update('docs', [
            'file' => $file,
        ], "`doc_id`='{$doc_id}'");
    }
    

    $result = DB::update('docs', [
        'doc_type_id' => post('doc_type_id'),
        'doc_name' => post('doc_name'),
        'to_user_id' => post('to_user_id'),
        'read_status' => 0
    ], "`doc_id`='{$doc_id}'");

    if ($result) {
        setAlert('success', "แก้ไขการส่งเอกสารสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขการส่งเอกสารได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `docs` WHERE `doc_id`='{$doc_id}'");
$doc_types = DB::result("SELECT * FROM `doc_types`");
$items = DB::result("SELECT * FROM `users` WHERE `user_type`='user' AND `status`=1 AND `user_id`!='{$user_id}'");
ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="doc_name">ชื่อเรื่อง</label>
    <input type="text" name="doc_name" id="doc_name" value="<?= $data['doc_name'] ?>" required>
    <br>

    <label for="file">ไฟล์เอกสาร</label>
    <input type="file" name="file" id="file">
    <br>

    <label for="doc_type_id">ประเภทเอกสาร</label>
    <select name="doc_type_id" id="doc_type_id" required>
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($doc_types as $item) : ?>
            <option value="<?= $item['doc_type_id'] ?>" <?= $item['doc_type_id'] === $data['doc_type_id'] ? 'selected' : null ?>><?= $item['doc_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for="to_user_id">ส่งถึง</label>
    <select name="to_user_id" id="to_user_id" required>
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['user_id'] ?>" <?= $item['user_id'] === $data['to_user_id'] ? 'selected' : null ?>><?= $item['firstname'] . ' ' . $item['lastname'] ?> (<?= $item['email'] ?>)</option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขการส่งเอกสารให้ผู้ใช้งานคนอื่น';
require ROOT . '/user/layout.php';
